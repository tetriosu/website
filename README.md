# Roadmap

- [x] Finish migration from Github to Gitlab (transfer all website data)
- [ ] Switch to [Astro Framework](https://astro.build)
- [ ] Migrate to [Tailwind CSS](https://tailwindcss.com) 
- [ ] Switch color palette
